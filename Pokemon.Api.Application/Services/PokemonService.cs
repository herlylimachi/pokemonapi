﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.Application.Services
{
    using Pokedex.Api.Application.Contracts.Services;
    using Pokedex.Api.business.Models;
    using Pokedex.Api.DataAccess.Contracts.Repositories;
    using Pokedex.Api.DataAccess.Mappers;
    using System.Threading.Tasks;

    public class PokemonService : IPokemonService
    {
        private readonly IPokemonRepository _pokemonRepository;

        public PokemonService(IPokemonRepository pokemonRepository)
        {
            _pokemonRepository = pokemonRepository;
        }
        public Task<Pokemon> AddPokemon(Pokemon pokemon)
        {

            throw new NotImplementedException();
        }

        public Task DeletePokemon(int id)
        {
            throw new NotImplementedException();
        }

        public Task<Pokemon> FindPokemones(string filtroBusqueda)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Pokemon>> GetAllPokemones()
        {
            var pokemones = await _pokemonRepository.GetAll();
            return PokemonMapper.Map(pokemones);
        }

        public async Task<Pokemon> GetPokemon(int id)
        {
            var pokemon = await _pokemonRepository.Get(id);
            return PokemonMapper.Map(pokemon);
        }

        public Task<Pokemon> UpdatePokemon(Pokemon pokemon)
        {
            throw new NotImplementedException();
        }
    }
}
