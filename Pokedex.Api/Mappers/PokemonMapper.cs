﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.Mappers
{
    using Pokedex.Api.business.Models;
    using Pokedex.Api.ViewModels;
    public static class PokemonMapper
    {
        public static Pokemon Map(PokemonModel model)
        {
            if (model == null) return null;
            return new Pokemon()
            {
                Id = model.Id

            };
        }

        public static PokemonModel Map(Pokemon businessModel)
        {
            if (businessModel == null) return null;

            return new PokemonModel()
            {
                Id = businessModel.Id,
                Name = NamePokemonMapper.Map(businessModel.Name),
                Type = businessModel.Type,
                Base = BasePokemonMapper.Map(businessModel.Base),
            };
        }

        public static List<PokemonModel> Map(List<Pokemon> businessModel)
        {
            if (businessModel == null) return null;
            List<PokemonModel> listPokemones = new List<PokemonModel>();
            foreach (var pokemon in businessModel)
            {
                listPokemones.Add(Map(pokemon));
            }
            return listPokemones;
        }
    }
}
