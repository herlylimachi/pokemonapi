﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.Mappers
{
    using Pokedex.Api.business.Models;
    using Pokedex.Api.ViewModels;
    public static class BasePokemonMapper
    {
        public static BasePokemon Map(BasePokemonModel model)
        {
            if (model == null) return null;
            
            return new BasePokemon()
            {
                Attack = model.Attack,
                Defense = model.Defense,
                Hp = model.Hp,
                SpAttack = model.SpAttack,
                SpDefense = model.SpDefense,
                Speed = model.Speed,
            };
        }

        public static BasePokemonModel Map(BasePokemon businessModel)
        {
            if (businessModel == null) return null;
            return new BasePokemonModel()
            {
                Attack = businessModel.Attack,
                Defense = businessModel.Defense,
                Hp = businessModel.Hp,
                SpAttack = businessModel.SpAttack,
                SpDefense = businessModel.SpDefense,
                Speed = businessModel.Speed,
            };
        }
    }
}
