﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.Mappers
{
    using Pokedex.Api.business.Models;
    using Pokedex.Api.ViewModels;

    public static class NamePokemonMapper
    {
        public static NamePokemon Map(NamePokemonModel model)
        {
            if (model == null) return null;
            return new NamePokemon()
            {
                Chinese = model.Chinese,
                English = model.English,
                French = model.French,
                Japanese = model.Japanese,

            };
        }

        public static NamePokemonModel Map(NamePokemon businessModel)
        {
            if (businessModel == null) return null;
            return new NamePokemonModel()
            {
                Chinese = businessModel.Chinese,
                English = businessModel.English,
                French = businessModel.French,
                Japanese = businessModel.Japanese,
            };
        }
    }
}
