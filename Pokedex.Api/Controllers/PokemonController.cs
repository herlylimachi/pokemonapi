﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Pokedex.Api.Controllers
{
    using Pokedex.Api.Application.Contracts.Services;
    using Pokedex.Api.Mappers;

    [Produces("application/json")]
    [Route("Pokemon")]
    [ApiController]
    public class PokemonController : ControllerBase
    {
        private readonly IPokemonService _pokemonServices;
        public PokemonController(IPokemonService pokemonServices)
        {
            _pokemonServices = pokemonServices;
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetPokemon(int id)
        {
            var pokemon = PokemonMapper.Map(await _pokemonServices.GetPokemon(id));

            if (pokemon == null)
            {
                return NotFound("Recurso no encontrado");
            }
            return Ok(pokemon);
        }

        [HttpGet()]
        public async Task<IActionResult> GetPokemones()
        {
            var pokemones = PokemonMapper.Map(await _pokemonServices.GetAllPokemones());            
            return Ok(pokemones);
        }
    }
}
