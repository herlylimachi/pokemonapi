﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.ViewModels
{
    public class PokemonModel
    {
        public int Id { get; set; }
        public NamePokemonModel Name { get; set; }
        public List<string> Type { get; set; }
        public BasePokemonModel Base { get; set; }
    }
}
