﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.ViewModels
{
    public class NamePokemonModel
    {
        public string English { get; set; }
        public string Japanese { get; set; }
        public string Chinese { get; set; }
        public string French { get; set; }
    }
}
