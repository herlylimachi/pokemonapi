#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src
COPY ["Pokedex.Api/Pokedex.Api.csproj", "Pokedex.Api/"]
COPY ["Pokemon.Api.CrossCutting/Pokedex.Api.CrossCutting.csproj", "Pokemon.Api.CrossCutting/"]
COPY ["Pokemon.Api.Application.Contracts/Pokedex.Api.Application.Contracts.csproj", "Pokemon.Api.Application.Contracts/"]
COPY ["Pokemon.Api.business/Pokedex.Api.business.csproj", "Pokemon.Api.business/"]
COPY ["Pokedex.Api.DataAccess/Pokedex.Api.DataAccess.csproj", "Pokedex.Api.DataAccess/"]
COPY ["Pokedex.Api.DataAccess.Contracts/Pokedex.Api.DataAccess.Contracts.csproj", "Pokedex.Api.DataAccess.Contracts/"]
COPY ["Pokemon.Api.Application/Pokedex.Api.Application.csproj", "Pokemon.Api.Application/"]
RUN dotnet restore "Pokedex.Api/Pokedex.Api.csproj"
COPY . .
WORKDIR "/src/Pokedex.Api"
RUN dotnet build "Pokedex.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "Pokedex.Api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "Pokedex.Api.dll"]