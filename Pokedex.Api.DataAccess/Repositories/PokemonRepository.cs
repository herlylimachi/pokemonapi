﻿using MongoDB.Driver;
using Pokedex.Api.DataAccess.Contracts.Entities;
using Pokedex.Api.DataAccess.Contracts.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Pokedex.Api.DataAccess.Repositories
{
    public class PokemonRepository : IPokemonRepository
    {
        public Task<PokemonEntity> Add(PokemonEntity element)
        {
            throw new NotImplementedException();
        }

        public Task AddRange(List<PokemonEntity> elements)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task DeleteRangeAsync(List<PokemonEntity> elements)
        {
            throw new NotImplementedException();
        }

        public Task<bool> Exist(int id)
        {
            throw new NotImplementedException();
        }

        public Task<List<PokemonEntity>> Find(System.Linq.Expressions.Expression<Func<PokemonEntity, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task<PokemonEntity> Get(int id)
        {
            var client = new MongoClient(Environment.GetEnvironmentVariable("MongoClient"));
            var database = client.GetDatabase(Environment.GetEnvironmentVariable("MongoDatabase"));
            var collection = database.GetCollection<PokemonEntity>(Environment.GetEnvironmentVariable("Mongocollection"));
            return await collection.Find(pokemon => pokemon.id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<PokemonEntity>> GetAll()
        {
            var client = new MongoClient(Environment.GetEnvironmentVariable("MongoClient"));
            var database = client.GetDatabase(Environment.GetEnvironmentVariable("MongoDatabase"));
            var collection = database.GetCollection<PokemonEntity>(Environment.GetEnvironmentVariable("Mongocollection"));
            return await collection.Find(item => true).ToListAsync();
        }

        public Task<PokemonEntity> Update(int id, PokemonEntity element)
        {
            throw new NotImplementedException();
        }
    }
}
