﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.DataAccess.Mappers
{
    using Pokedex.Api.business.Models;
    using Pokedex.Api.DataAccess.Contracts.Entities;

    public static class NamePokemonMapper
    {
        public static NamePokemon Map(NamePokemonEntity entity)
        {
            if (entity == null) return null;
            return new NamePokemon()
            {
                Chinese = entity.chinese,
                English = entity.english,
                French = entity.french,
                Japanese = entity.japanese,

            };     
        }  
    }
}
