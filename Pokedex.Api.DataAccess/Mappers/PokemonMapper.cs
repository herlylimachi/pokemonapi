﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.DataAccess.Mappers
{
    using Pokedex.Api.business.Models;
    using Pokedex.Api.DataAccess.Contracts.Entities;

    public static class PokemonMapper
    {   

        public static Pokemon Map(PokemonEntity entity)
        {
            if (entity == null) return null;

            return new Pokemon()
            {
                Id = entity.id,
                Name = NamePokemonMapper.Map(entity.name),
                Type = entity.type,
                Base = BasePokemonMapper.Map(entity.Base),
            };
        }

        public static List<Pokemon> Map(IEnumerable<PokemonEntity> entity)
        {
            if (entity == null) return null;
            List<Pokemon> listPokemones = new List<Pokemon>();
            foreach (var pokemon in entity)
            {
                listPokemones.Add(Map(pokemon));
            }
            return listPokemones;
        }
    }
}
