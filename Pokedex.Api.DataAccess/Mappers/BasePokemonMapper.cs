﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Pokedex.Api.DataAccess.Mappers
{
    using Pokedex.Api.business.Models;
    using Pokedex.Api.DataAccess.Contracts.Entities;
    public static class BasePokemonMapper
    {
        public static BasePokemon Map(BasePokemonEntity entity)
        {
            if (entity == null) return null;
            
            return new BasePokemon()
            {
                Attack = entity.Attack,
                Defense = entity.Defense,
                Hp = entity.HP,
                SpAttack = entity.SpAttack,
                SpDefense = entity.SpDefense,
                Speed = entity.Speed,
            };
        }
    }
}
