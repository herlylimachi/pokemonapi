﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.CrossCutting.Register
{
    using Pokedex.Api.Application.Contracts.Services;
    using Pokedex.Api.Application.Services;
    using Microsoft.Extensions.DependencyInjection;
    using Pokedex.Api.DataAccess.Contracts;
    using Pokedex.Api.DataAccess;
    using Microsoft.Extensions.Options;
    using Microsoft.Extensions.Configuration;
    using Pokedex.Api.DataAccess.Contracts.Repositories;
    using Pokedex.Api.DataAccess.Repositories;

    public static class IoCRegister
    {
        public static IServiceCollection AddServices(this IServiceCollection services, IConfiguration configuration)
        {
            Environment.GetEnvironmentVariable("windir");          
            services.AddTransient<IPokemonService, PokemonService>();
            services.AddTransient<IPokemonRepository, PokemonRepository>();
            return services;


        }


    }
}
