﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.Application.Contracts.Services
{
    using Pokedex.Api.business.Models;
    using System.Threading.Tasks;

    public interface IPokemonService
    {
        Task<List<Pokemon>> GetAllPokemones();
        Task<Pokemon> GetPokemon(int id);
        Task<Pokemon> AddPokemon(Pokemon pokemon);
        Task DeletePokemon(int id);
        Task<Pokemon> UpdatePokemon(Pokemon pokemon);
        Task<Pokemon> FindPokemones(string filtroBusqueda);
    }
}
