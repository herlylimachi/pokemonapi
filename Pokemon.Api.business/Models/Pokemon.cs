﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.business.Models
{
    public class Pokemon
    {
        public int Id { get; set; }
        public NamePokemon Name { get; set; }
        public List<string> Type { get; set; }
        public BasePokemon Base { get; set; }
    }
}
