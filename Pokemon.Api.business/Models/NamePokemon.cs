﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.business.Models
{
    public class NamePokemon
    {   
        public string English { get; set; }
        public string Japanese { get; set; }
        public string Chinese { get; set; }
        public string French { get; set; }
    }
}
