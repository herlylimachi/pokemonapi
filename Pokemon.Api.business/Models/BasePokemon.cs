﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.business.Models
{
    public class BasePokemon
    {
        public int Hp { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int SpAttack { get; set; }
        public int SpDefense { get; set; }
        public int Speed { get; set; }

    }
}
