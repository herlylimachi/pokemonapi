﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.DataAccess.Contracts.Entities
{
    [BsonIgnoreExtraElements]
    public class PokemonEntity
    {        
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public int id { get; set; }
        public NamePokemonEntity name { get; set; }
        public List<string> type { get; set; }
        public BasePokemonEntity Base { get; set; }
    }
}
