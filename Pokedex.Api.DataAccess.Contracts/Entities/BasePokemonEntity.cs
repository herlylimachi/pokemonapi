﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.DataAccess.Contracts.Entities
{
    public class BasePokemonEntity
    {
        public int HP { get; set; }
        public int Attack { get; set; }
        public int Defense { get; set; }
        public int SpAttack { get; set; }
        public int SpDefense { get; set; }
        public int Speed { get; set; }
    }
}
