﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.DataAccess.Contracts.Entities
{
    public class NamePokemonEntity
    {
        public string english { get; set; }
        public string japanese { get; set; }
        public string chinese { get; set; }
        public string french { get; set; }
    }
}
