﻿using Pokedex.Api.DataAccess.Contracts.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pokedex.Api.DataAccess.Contracts.Repositories
{
    public interface IPokemonRepository : IRepositoryBase<PokemonEntity>
    {
     
    }
}
