﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Pokedex.Api.DataAccess.Contracts.Repositories
{
    public interface IRepositoryBase<T> where T : class
    {
        Task<T> Add(T element);
        Task AddRange(List<T> elements);
        Task DeleteAsync(int id);
        Task DeleteRangeAsync(List<T> elements);
        Task<bool> Exist(int id);
        Task<List<T>> Find(Expression<Func<T, bool>> predicate);
        Task<T> Get(int id);
        Task<IEnumerable<T>> GetAll();
        Task<T> Update(int id, T element);
    }
}
